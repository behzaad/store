#include<stdio.h>
#include<stdlib.h>
typedef struct producta{
  int id;
  char type[20];
  char name[20];
  double shop_price;
  double sell_price;
  double sell_rate;
  int stock;
}product;
void main_menu(void);
void inventory_menu(void);
void serch_menu(void);
void show_product(product);
void remove_product(void);
void sell_menu(void);
void sell_serch_id(void);
void sell_serch_tyna(void);
void shop_menu(void);
void financial(void);
void purchase(double);
void income(void);
void sold_show(void);
void sold(product);
void sale(product);
int main(){
  main_menu();
}
void main_menu(void){
  int n;
  printf("1.INVENTORY\n\n");
  printf("2.SELL\n\n");
  printf("3.SHOP\n\n");
  printf("4.FINANCIAL\n\n");
  printf("0.exit\n\n");
  scanf("%d",&n);
  if(n==1) inventory_menu();
  if(n==2) sell_menu();
  if(n==3) shop_menu();
  if(n==4) financial();
  if(n==0) exit(0);
}
void inventory_menu(){
  int n;
  product a;
  FILE *shop;
  shop=fopen("testf.bin","rb+");
  printf("1.SHOW PRODUCT\n\n");
  printf("2.REMOVE PRODUCT\n\n");
  printf("3.SERCH\n\n");
  printf("0.BACK\n\n");
  scanf("%d",&n);
  if(n==1){
    for(;!feof(shop);){
      fread(&a,sizeof(product),1,shop);
      if(feof(shop)) break;
      show_product(a);
    }
   fclose(shop);
   main_menu();
  }
  if(n==2) remove_product();
  if(n==3) serch_menu();
  if(n==0) main_menu();
}
void serch_menu(void){
  int n,i,j,id;
  double e,s;
  char t[20];
  product a;
  FILE *shop;
  shop=fopen("testf.bin","rb+");
  if(!shop){
    printf("can't open the file\n ");
    return ;
  }
  printf("SERCH BY :\n");
  printf("1.PRODUCT TYPE \n\n");
  printf("2.PRODUCT TYPE PRICE\n\n");
  printf("3.PRODUCT NAME \n\n");
  printf("4.PRODUCT PRICE\n\n");
  printf("5.PRODUCT ID\n\n");
  printf("0.BACK\n\n");
  scanf("%d",&n);
  if(n==1){
    printf("ENTER PRODUCT TYPE:\t\t");
    scanf("%s",t);
    for(i=0;t[i];i++);
    for(;!feof(shop);){
      fread(&a,sizeof(product),1,shop);
        for(j=0;a.type[j]==t[j];j++){
          if(j==i){
            show_product(a);
            fread(&a,sizeof(product),1,shop);
            if(feof(shop)) main();
            j=0;
          }
        }
     }
  }
  if(n==2){
    printf("ENTER PRODUCT TYPE:\t\t");
    scanf("%s",t);
    printf("ENTER YOUR START PRICE:\t\t");
    scanf("%lf",&s);
    printf("ENTER YOUR END PRICE:\t\t");
    scanf("%lf",&e);
      for(i=0;t[i];i++);
      for(;!feof(shop);){
        fread(&a,sizeof(product),1,shop);
        for(j=0;a.type[j]==t[j];j++){
          if(j==i && e>=a.sell_price && s<=a.sell_price){
            show_product(a);
            fread(&a,sizeof(product),1,shop);
            if(feof(shop)) main();
            j=0;
          }
        }
     }
     main();
  }
  if(n==3){
    printf("ENTER PRODUCT TYPE:\t\t");
    scanf("%s",t);
    for(;!feof(shop);){
      fread(&a,sizeof(product),1,shop);
        for(i=0;t[i];i++);
        for(j=0;a.name[j]==t[j];j++){
          if(j==i){
            show_product(a);
            fread(&a,sizeof(product),1,shop);
            if(feof(shop)) main();
            j=0;
          }
        }
    }
  }
  if(n==4){
    printf("ENTER PRODUCT :\t\t");
    scanf("%s",t);
    printf("ENTER YOUR START PRICE:\t\t");
    scanf("%lf",&s);
    printf("ENTER YOUR END PRICE:\t\t");
    scanf("%lf",&e);
    for(;!feof(shop);){
      fread(&a,sizeof(product),1,shop);
        for(i=0;t[i];i++);
        for(j=0;a.name[j]==t[j];j++){
          if(j==i && e>=a.sell_price && s<=a.sell_price){
            show_product(a);
            fread(&a,sizeof(product),1,shop);
            if(feof(shop)) main();
            j=0;
          }
        }
    }
  }
  if(n==5){
    printf("ENTER PRODUCT ID\t\t");
    scanf("%d",&id);
    for(;!feof(shop);){
      fread(&a,sizeof(product),1,shop);
      if(feof(shop)) break;
      if(id==a.id){
        show_product(a);
      }
    }
  }
  fclose(shop);
  if(n==0) inventory_menu();
  main_menu();
}
void shop_menu(void){
  int n;
  product a;
  FILE *shop;
  shop=fopen("testf.bin","ab+");
  if(!shop){
    printf("can't open the file\n ");
    return ;
  }
  printf("HOW MANY PRODUCT YOU WANT TO SHOP\n");
  scanf("%d",&n);
  for(;n>0;n--){
    printf("FILL THE FILEDS\n");
    printf("PRODUCT ID:\t");
    scanf("%d",&a.id);
    printf("PRODUCT TYPE:\t");
    scanf("%s",a.type);
    printf("PRODUCT NAME:\t");
    scanf("%s",a.name);
    printf("PRODUCT SHOP_PRICE:\t");
    scanf("%lf",&a.shop_price);
    printf("PRODUCT SELL_PRICE:\t");
    scanf("%lf",&a.sell_price);
    printf("PRODUCT SELL_RATE:\t");
    scanf("%lf",&a.sell_rate);
    printf("PRODUCT STOCK:\t");
    scanf("%d",&a.stock);
    purchase(a.shop_price);
    fwrite(&a,sizeof(a),1,shop);
  }
  fclose(shop);
  main_menu();
}
void sell_menu(void){
    int n;
    printf("WHAT PRODUCT YOU WANT TO SELL\n\n");
    printf("SERCH THE PRODUCT:\n\n");
    printf("1.BY PRODUCT ID\n\n");
    printf("2.BY PRODUCT TYPE AND NAME\n\n");
    printf("0.BACK\n\n");
    scanf("%d",&n);
    if(n==1) sell_serch_id();
    if(n==2) sell_serch_tyna();
    if(n==0) main_menu();
  }
void show_product(product a){
  printf("PRODUCT NUMBER:\t");
  printf("%d\n",a.id);
  printf("PRODUCT TYPE:\t");
  printf("%s\n",a.type);
  printf("PRODUCT NAME:\t");
  printf("%s\n",a.name);
  printf("PRODUCT SHOP_PRICE:\t");
  printf("%lf\n",a.shop_price);
  printf("PRODUCT SELL_PRICE:\t");
  printf("%lf\n",a.sell_price);
  printf("PRODUCT SELL_RATE:\t");
  printf("%lf\n",a.sell_rate);
  printf("PRODUCT STOCK:\t");
  printf("%d\n",a.stock);
  printf("******\n\n");
}
void sell_serch_id(void){
  product a;
  int id;
  int n;
  int x=1;
  FILE *shop;
  shop=fopen("testf.bin","r+b");
  if(!shop){
    printf("can't open the file\n ");
    return ;
  }
  printf("PRODUCT ID:\t\t");
  scanf("%d",&id);
  printf("HOW MANY DO YOU WANT TO SELL THIS PRODUCT?\t\t");
  scanf("%d",&n);
  for(;!feof(shop);){
    fread(&a,sizeof(product),1,shop);
    for(;id==a.id && n>0;){
      if(a.stock==0 && x==1){
        printf("THIS PRODUCT IS NOT AVAILABLE\n\n");
        return ;
      }
      if(a.stock==0 && x==0) main();
      if(a.stock<n && x==1){
        printf("%d NUMBER OF THIS PRODUCT IS AVIALABLE\n",a.stock);
        printf("IF YOU WANT TO SELL ENTER 1 OTHERWISE ENTER 2:\t\t");
        scanf("%d",&x);
        if(x==1) x=0;
        if(x==2) main();
      }
      sale(a);
      a.stock--;
      n--;
      }
    }
  sold(a);
  fclose(shop);
  main_menu();
}
void sell_serch_tyna(void){
  product a;
  int id,i,j,k,l;
  char na[20];
  char t[20];
  int n;
  int x=1;
  FILE *shop;
  shop=fopen("testf.bin","rb+");
  if(!shop){
    printf("can't open the file\n ");
    return ;
  }
  printf("PRODUCT TYPE:\t\t");
  scanf("%s",t);
  printf("PRODUCT NAME:\t\t");
  scanf("%s",na);
  printf("HOW MANY DO YOU WANT TO SELL THIS PRODUCT?\t\t");
  scanf("%d",&n);
  for(;!feof(shop);){
    fread(&a,sizeof(product),1,shop);
    for(i=0;t[i];i++);
    for(j=0;na[j];j++);
    for(k=0;t[k]==a.type[k];k++){
      if(i==k){
        for(l=0;na[l]==a.name[l];l++){
          for(;j==l && n>0;){
            if(a.stock==0 && x==1){
              printf("THIS PRODUCT IS NOT AVAILABLE\n\n");
              return ;
            }
            if(a.stock==0 && x==0) main();
            if(a.stock<n && x==1){
              printf("%d NUMBER OF THIS PRODUCT IS AVIALABLE\n",a.stock);
              printf("IF YOU WANT TO SELL ENTER 1 OTHERWISE ENTER 2:\t\t");
              scanf("%d",&x);
              if(x==1) x=0;
              if(x==2) main();
            }
            sale(a);
            a.stock--;
            n--;
           }
         }
       }
     }
   }
   sold(a);
   fclose(shop);
   main_menu();
}
void remove_product(void){
  FILE *shop;
  FILE *shop2;
  product a;
  int id;
  shop2=fopen("x33.bin","wb+");
  shop=fopen("testf.bin","rb+");
  if(!shop){
    printf("can't open the file 24\n ");
    return;
  }
  printf("PRODUCT ID:\t\t");
  scanf("%d",&id);
  for(;!feof(shop);){
    fread(&a,sizeof(product),1,shop);
    if(feof(shop)){
      break;
    }
    if(a.id==id){
      continue;
    }
    fwrite(&a,sizeof(product),1 ,shop2);
  }
  fclose(shop);
  fclose(shop2);
  shop2=fopen("x33.bin","rb+");
  shop=fopen("testf.bin","wb+");
  for(;!feof(shop2);){
    fread(&a,sizeof(product),1,shop2);
    if(feof(shop2)){
      break;
    }
    fwrite(&a,sizeof(product),1,shop);
  }
  fclose(shop);
  fclose(shop2);
  main_menu();
}
void purchase(double a){
  FILE *purchase;
  purchase=fopen("testf1.bin","ab+");
  fwrite(&a,sizeof(double),1,purchase);
  fclose(purchase);
}
void income(void){
  FILE *sale;
  double a,b,sum1=0,sum2=0,sum3;
  FILE *purchase;
  sale=fopen("testf2.bin","rb+");
  purchase=fopen("testf1.bin","rb+");
  if(!sale){
    printf("can't open the file 192\n ");
    return ;
  }
  if(!purchase){
    printf("can't open the file 196\n ");
    return ;
  }
  for(;!feof(sale);){
    fread(&a,sizeof(double),1,sale);
    if(feof(sale)) break;
    sum1=sum1+a;
  }
  for(;!feof(purchase);){
    fread(&b,sizeof(double),1,purchase);
    if(feof(purchase)) break;
    sum2=sum2+b;
  }
  sum3=sum1-sum2;
  printf("TOTAL SALE OF THIS STORE UNTILE NOW IS %lf\n\n",sum1);
  printf("TOTAL PURCHASE OF THIS STORE UNTILE NOW IS %lf\n\n",sum2);
  printf("TOTLA INCOME OF THIS STORE UNTILE NOW IS %lf\n\n",sum3);
  fclose(sale);
  fclose(purchase);
  main();
}
void sale(product b){
  FILE *sale;
  double a;
  a=b.sell_price-b.sell_rate;
  sale=fopen("testf2.bin","ab+");
  if(!sale){
    printf("can't open the file 139\n ");
    return ;
  }
  fwrite(&a,sizeof(a),1,sale);
  fclose(sale);
}
void sold(product a){
  FILE *sold;
  sold=fopen("testf3.bin","ab+");
  if(!sold){
    printf("can't open the file 149\n ");
    return ;
  }
  fwrite(&a,sizeof(product),1,sold);
  fclose(sold);
}
void sold_show(void){
  FILE *sold;
  product a;
  sold=fopen("testf3.bin","rb+");
  if(!sold){
    printf("can't open the file 159\n ");
    return ;
  }
  for(;!feof(sold);){
    fread(&a,sizeof(product),1,sold);
    if(feof(sold)) break;
    show_product(a);
  }
  fclose(sold);
  main();
}
void financial(void){
  int n;
  printf("1.SHOW INCOME\n\n");
  printf("2.LIST OF SOLD\n\n");
  printf("0.BACK\n\n");
  scanf("%d",&n);
  if(n==1) income();
  if(n==2) sold_show();
  if(n==3) main();
}
